﻿namespace AutoRent.Common.Interfaces
{
    public interface IValidation
    {
        void Error(string key, string msg);
        bool IsValid { get; }
    }
}
