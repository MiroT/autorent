﻿using AutoRent.BaseService.Domain;
using AutoRent.Enumerations.CarRentEnums;
using AutoRent.RelationalServices.Domain.UserModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace AutoRent.RelationalServices.Domain.CarModel
{
    public class CarRent : BaseModel
    {
        [Required]
        public string Name { get; set; }

        //Some notes about the renting of the car
        public string Note { get; set; }

        //The beggining date of rent
        [Required]
        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }

        //The end date of the rent
        [Required]
        [Display(Name = "End date")]
        public DateTime EndDate { get; set; }

        //The calculated price for the selected days
        [Required]
        [Display(Name = "Total price")]
        public decimal TotalPrice { get; set; }

        //The place from where the car is rent
        [Required]
        [Display(Name = "Rental address")]
        public Address RentAddress { get; set; }

        //The place from where the car is rent
        [Required]
        [Display(Name = "Return address")]
        public Address ReturnAddress { get; set; }

        //The navigation properties
        public int UserId { get; set; }
        public int? CarId { get; set; }

        //The connections of this model with the other models
        public virtual User User { get; set; }
        public virtual Car Car { get; set; }
    }
}
