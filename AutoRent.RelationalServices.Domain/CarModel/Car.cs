﻿using AutoRent.BaseService.Domain;
using AutoRent.Enumerations.CarEnums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AutoRent.RelationalServices.Domain.CarModel
{
    public class Car : BaseModel
    {
        [Required]
        public string Brand { get; set; }

        [Required]
        public string Model { get; set; }

        [Required]
        public string Color { get; set; }

        [Display(Name = "Image URL")]
        public string ImgURL { get; set; }

        [Required]
        [Range(2,7)]
        [Display(Name = "Number of seats")]
        public int NumberOfSeats { get; set; }

        //The exact price of the car on the market
        [Required]
        [Range(500,100000)]
        [Display(Name = "Car price")]
        public decimal PriceOfCar { get; set; }

        //The price of rent per day of the car
        [Required]
        [Display(Name = "Price of rent per day")]
        public decimal PriceOfRentPerDay { get; set; }

        [Required]
        [Range(40,100)]
        [Display(Name = "Fuel capacity")]
        public int FuelCapacity { get; set; }

        [Required]
        [Range(2,5)]
        [Display(Name = "Number of doors")]
        public int NumberOfDoors { get; set; }

        [Required]
        [Display(Name = "Fuel type")]
        public FuelType FuelType { get; set; }

        //The number of cars of this model in the store and its branches
        [Required]
        [Range(1,100)]
        [Display(Name = "Avaliable cars for this model")]
        public int Amount { get; set; }

        //The connections of this model with the other models
        public virtual ICollection<CarRent> CarRent { get; set; }
    }
}
