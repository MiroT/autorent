﻿namespace AutoRent.Enumerations.CarEnums
{
    //Enum for the different fuel types of the cars
    public enum FuelType
    {
        Diesel,
        Gasoline,
        Liquefied_Petroleum,
        Compressed_Natural_Gas,
        Ethanol,
        Bio_Diesel,
        Hybrid
    }
}
