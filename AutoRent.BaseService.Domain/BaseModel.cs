﻿namespace AutoRent.BaseService.Domain
{
    public class BaseModel
    {
        public int Id { get; set; }
    }
}
