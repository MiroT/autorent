// <auto-generated />
namespace RentACar.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AdminRolesUpdatedMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AdminRolesUpdatedMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201803120855064_AdminRolesUpdatedMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
