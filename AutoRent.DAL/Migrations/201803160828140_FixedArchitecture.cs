namespace RentACar.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedArchitecture : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.CarRents", "UserId");
            CreateIndex("dbo.CarRents", "CarId");
            CreateIndex("dbo.Payments", "UserId");
            AddForeignKey("dbo.CarRents", "CarId", "dbo.Cars", "Id");
            AddForeignKey("dbo.CarRents", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Payments", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "UserId", "dbo.Users");
            DropForeignKey("dbo.CarRents", "UserId", "dbo.Users");
            DropForeignKey("dbo.CarRents", "CarId", "dbo.Cars");
            DropIndex("dbo.Payments", new[] { "UserId" });
            DropIndex("dbo.CarRents", new[] { "CarId" });
            DropIndex("dbo.CarRents", new[] { "UserId" });
        }
    }
}
