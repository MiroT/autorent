namespace RentACar.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedArchitecture : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CarRents", "CarId", "dbo.Cars");
            DropForeignKey("dbo.CarRents", "UserId", "dbo.Users");
            DropForeignKey("dbo.Payments", "UserId", "dbo.Users");
            DropIndex("dbo.CarRents", new[] { "UserId" });
            DropIndex("dbo.CarRents", new[] { "CarId" });
            DropIndex("dbo.Payments", new[] { "UserId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Payments", "UserId");
            CreateIndex("dbo.CarRents", "CarId");
            CreateIndex("dbo.CarRents", "UserId");
            AddForeignKey("dbo.Payments", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CarRents", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CarRents", "CarId", "dbo.Cars", "Id");
        }
    }
}
