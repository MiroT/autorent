namespace RentACar.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminRolesUpdatedMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CarRents", "AdminId", "dbo.Admins");
            DropForeignKey("dbo.Payments", "AdminId", "dbo.Admins");
            DropForeignKey("dbo.CarRents", "UserId", "dbo.Users");
            DropForeignKey("dbo.Payments", "UserId", "dbo.Users");
            DropIndex("dbo.CarRents", new[] { "UserId" });
            DropIndex("dbo.CarRents", new[] { "AdminId" });
            DropIndex("dbo.Payments", new[] { "UserId" });
            DropIndex("dbo.Payments", new[] { "AdminId" });
            AlterColumn("dbo.CarRents", "UserId", c => c.Int(nullable: false));
            AlterColumn("dbo.Payments", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.CarRents", "UserId");
            CreateIndex("dbo.Payments", "UserId");
            AddForeignKey("dbo.CarRents", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Payments", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            DropColumn("dbo.CarRents", "AdminId");
            DropColumn("dbo.Payments", "AdminId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Payments", "AdminId", c => c.Int());
            AddColumn("dbo.CarRents", "AdminId", c => c.Int());
            DropForeignKey("dbo.Payments", "UserId", "dbo.Users");
            DropForeignKey("dbo.CarRents", "UserId", "dbo.Users");
            DropIndex("dbo.Payments", new[] { "UserId" });
            DropIndex("dbo.CarRents", new[] { "UserId" });
            AlterColumn("dbo.Payments", "UserId", c => c.Int());
            AlterColumn("dbo.CarRents", "UserId", c => c.Int());
            CreateIndex("dbo.Payments", "AdminId");
            CreateIndex("dbo.Payments", "UserId");
            CreateIndex("dbo.CarRents", "AdminId");
            CreateIndex("dbo.CarRents", "UserId");
            AddForeignKey("dbo.Payments", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.CarRents", "UserId", "dbo.Users", "Id");
            AddForeignKey("dbo.Payments", "AdminId", "dbo.Admins", "Id");
            AddForeignKey("dbo.CarRents", "AdminId", "dbo.Admins", "Id");
        }
    }
}
