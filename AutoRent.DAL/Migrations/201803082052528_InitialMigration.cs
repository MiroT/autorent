namespace RentACar.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 20),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CarRents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Note = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RentAddress = c.Int(nullable: false),
                        ReturnAddress = c.Int(nullable: false),
                        UserId = c.Int(),
                        AdminId = c.Int(),
                        CarId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Admins", t => t.AdminId)
                .ForeignKey("dbo.Cars", t => t.CarId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.AdminId)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        Color = c.String(nullable: false),
                        ImgURL = c.String(),
                        NumberOfSeats = c.Int(nullable: false),
                        PriceOfCar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceOfRentPerDay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FuelCapacity = c.Int(nullable: false),
                        NumberOfDoors = c.Int(nullable: false),
                        FuelType = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 20),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 20),
                        Email = c.String(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        PhoneNumber = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        AdditionalInformation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberOfCard = c.String(nullable: false, maxLength: 16),
                        SecurityNumber = c.String(nullable: false, maxLength: 3),
                        Date = c.DateTime(nullable: false),
                        CardType = c.Int(nullable: false),
                        CardExpiresOn = c.DateTime(nullable: false),
                        IsMainPaymentMethod = c.Boolean(nullable: false),
                        UserId = c.Int(),
                        AdminId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Admins", t => t.AdminId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.AdminId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "UserId", "dbo.Users");
            DropForeignKey("dbo.Payments", "AdminId", "dbo.Admins");
            DropForeignKey("dbo.CarRents", "UserId", "dbo.Users");
            DropForeignKey("dbo.CarRents", "CarId", "dbo.Cars");
            DropForeignKey("dbo.CarRents", "AdminId", "dbo.Admins");
            DropIndex("dbo.Payments", new[] { "AdminId" });
            DropIndex("dbo.Payments", new[] { "UserId" });
            DropIndex("dbo.CarRents", new[] { "CarId" });
            DropIndex("dbo.CarRents", new[] { "AdminId" });
            DropIndex("dbo.CarRents", new[] { "UserId" });
            DropTable("dbo.Payments");
            DropTable("dbo.Users");
            DropTable("dbo.Cars");
            DropTable("dbo.CarRents");
            DropTable("dbo.Admins");
        }
    }
}
