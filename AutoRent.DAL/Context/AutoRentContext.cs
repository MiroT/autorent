namespace RentACar.DAL.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using AutoRent.Common.Interfaces;
    using AutoRent.UserServices.Domain;
    using AutoRent.RelationalServices.Domain.UserModel;
    using AutoRent.RelationalServices.Domain.CarModel;
    using AutoRent.RelationalServices.Domain.PaymentModel;

    public partial class AutoRentContext : DbContext
    {
        public AutoRentContext()
            : base("name=AutoRentContext")
        {
        }

        public DbSet<Admin> Admin { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Car> Car { get; set; }
        public DbSet<CarRent> CarRent { get; set; }
        public DbSet<Payment> Payment { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
