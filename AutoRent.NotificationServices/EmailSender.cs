﻿using System.Net;
using System.Net.Mail;

namespace AutoRent.NotificationServices
{
    public class EmailSender
    {
        public SmtpClient Client { get; set; }

        //Send email using mailtrap.io
        public EmailSender()
        {
            Client = new SmtpClient("smtp.mailtrap.io", 2525)
            {
                Credentials = new NetworkCredential("2d1c8f75a4d370", "f29c1fb4af7f04"),
                EnableSsl = true
            };
        }

        public bool SendMail(string from, string subject, string body)
        {
            try
            {
                Client.Send(from, "admin@autorent.com", subject, body);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }
    }
}
