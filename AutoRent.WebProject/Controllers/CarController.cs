﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoRent.DAL.UnitOfWork;
using AutoRent.Validation;
using AutoRent.Authorization;
using AutoRent.CarServices;
using AutoRent.RelationalServices.Domain.CarModel;

namespace AutoRent.Controllers
{
    public class CarController : Controller
    {
        private CarService carService;

        public CarController()
        {
            carService = new CarService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        // GET: Car
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await carService.GetAllAsync());
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            Car car = carService.Get(id);

            if (car == null)
            {
                return RedirectToAction("Error", "Home");
            }

            return View(car);
        }

        [HttpGet, AuthorizationActionFilter(false)]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(Car car)
        {
            if (ModelState.IsValid)
            {
                carService.Insert(car);
                await carService.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            return View("Create");
        }

        [HttpGet, AuthorizationActionFilter(false)]
        public async Task<ActionResult> Update(int id)
        {
            return View(await carService.GetAsync(c => c.Id == id));
        }
        [HttpPost]
        public async Task<ActionResult> Update(Car car)
        {
            if (ModelState.IsValid)
            {
                carService.Update(car);
                await carService.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            return View("Update");
        }

        [HttpGet, AuthorizationActionFilter(false)]
        public async Task<ActionResult> Delete(int id)
        {
            return View(await carService.GetAsync(c => c.Id == id));
        }
        [HttpPost]
        public async Task<ActionResult> Delete(Car car)
        {
            //Calls the SetAllReferencesToNullWhenDeleted() method in the service
            carService.SetAllReferencesToNullWhenDeleted(car);
            carService.Delete(car);

            await carService.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}