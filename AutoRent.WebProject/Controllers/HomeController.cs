﻿using AutoRent.NotificationServices;
using AutoRent.WebProject.ViewModels;
using System.Web.Mvc;

namespace AutoRent.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Contacts()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contacts(EmailSendingViewModel emailSendingViewModel)
        {
            EmailSender emailSender = new EmailSender();

            if (emailSendingViewModel.Body == null || emailSendingViewModel.From == null || emailSendingViewModel.Subject == null)
            {
                TempData["Email"] = "You have to enter all the needed information for sending an email!";
                return View("Contacts");
            }

            if (emailSender.SendMail(emailSendingViewModel.From, emailSendingViewModel.Subject, emailSendingViewModel.Body))
            {
                TempData["Email"] = "You have send the email successfully!";
            }
            else
            {
                TempData["Email"] = "The email credentials have expired!";
            }

            return View("Contacts");
        }

        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EmptyCarAmount()
        {
            return View();
        }

        [HttpGet]
        public ActionResult News()
        {
            return View();
        }
    }
}