﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoRent.DAL.UnitOfWork;
using AutoRent.Validation;
using AutoRent.Authorization;
using AutoRent.CarServices;
using AutoRent.RelationalServices.Domain.CarModel;
using AutoRent.RelationalServices.Domain.UserModel;

namespace AutoRent.Controllers
{
    public class CarRentController : Controller
    {
        private CarRentService carRentService;

        public CarRentController()
        {
            carRentService = new CarRentService(new UnitOfWork(), new ValidationDictionary(ModelState));
        }

        // GET: CarRent
        [HttpGet, AuthorizationActionFilter(false)]
        public async Task<ActionResult> Index()
        {
            return View("ListAllCarRents", await carRentService.GetAllAsync());
        }

        [HttpGet, AuthorizationActionFilter(true)]
        public async Task<ActionResult> Details(int id)
        {
            CarRent carRent = carRentService.Get(id);
            //If payment does not exsists
            if (carRent == null)
            {
                return RedirectToAction("Error", "Home");
            }

            if (!AuthenticationController.IsLoggedAdmin)
            {
                //If this is not this user's payment
                if (!await carRentService.IsUsersCarRent(carRent, AuthenticationController.logged))
                {
                    return RedirectToAction("Error", "Home");
                }
            }

            return View(carRent);
        }

        [HttpGet, AuthorizationActionFilter(true)]
        public async Task<ActionResult> ListCurrentUserCarRents()
        {
            return View("Index", await carRentService.GetAllAsync(p => p.UserId == AuthenticationController.logged.Id));
        }

        [HttpGet, AuthorizationActionFilter(true)]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create(CarRent carRent, int id)
        {
            carRent.CarId = id;

            if (carRentService.CheckIfCarAmountIsZero(id))
            {
                return RedirectToAction("EmptyCarAmount", "Home");
            }

            if (carRentService.CheckIfMainPaymentIsNotSet((User)AuthenticationController.logged))
            {
                TempData["Payment"] = "You have to set a main payment method";

                return View("Create");
            }

            //Calls the validation method in the service
            carRentService.Validation(carRent);

            if (ModelState.IsValid)
            {
                carRent.UserId = AuthenticationController.logged.Id;

                carRentService.CalculateTotalPrice(carRent);

                carRentService.ReduceTheNumberOfAvaliableCars(carRent);

                carRentService.Insert(carRent);

               await carRentService.SaveChangesAsync();

                if (AuthenticationController.IsLoggedAdmin)
                {
                    return RedirectToAction("ListCurrentAdminCarRents");
                }

                return RedirectToAction("ListCurrentUserCarRents");
            }
            return View("Create");
        }

        [HttpGet, AuthorizationActionFilter(false)]
        public async Task<ActionResult> Update(int id)
        {
            TempData["CarRentId"] = id;
            return View(await carRentService.GetAsync(cr => cr.Id == id));
        }
        [HttpPost]
        public async Task<ActionResult> Update(CarRent carRent)
        {
            //Calls the validation method in the service
            carRentService.Validation(carRent);

            //Sets the CarRent to the original owner properties after update
            carRentService.SetCarRentPropertiesOnUpdate(carRent, int.Parse(TempData["CarRentId"].ToString()));

            if (ModelState.IsValid)
            {
                carRentService.Update(carRent);

                await carRentService.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View("Update");
        }

        [HttpGet, AuthorizationActionFilter(false)]
        public async Task<ActionResult> Delete(int id)
        {
            return View(await carRentService.GetAsync(cr => cr.Id == id));
        }
        [HttpPost]
        public async Task<ActionResult> Delete(CarRent carRent)
        {
            carRentService.Delete(carRent);

            await carRentService.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}