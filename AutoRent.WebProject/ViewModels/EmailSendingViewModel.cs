﻿namespace AutoRent.WebProject.ViewModels
{
    public class EmailSendingViewModel
    {
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}