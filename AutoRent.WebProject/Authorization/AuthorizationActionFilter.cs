﻿using AutoRent.Controllers;
using System.Web.Mvc;
using System.Web.Routing;

namespace AutoRent.Authorization
{
    public class AuthorizationActionFilter : ActionFilterAttribute
    {
        private bool user;

        public AuthorizationActionFilter(bool user)
        {
            this.user = user;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (AuthenticationController.logged == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {{"controller", "Home"}, {"action", "Error"}});

                filterContext.HttpContext.Response.StatusCode = 401;

                base.OnActionExecuting(filterContext);

                return;
            }

            //if it is not logged admin and it is forbidden for user
            if (!AuthenticationController.IsLoggedAdmin)
            {
                if (!user)
                {
                    filterContext.Result = new RedirectToRouteResult(
                     new RouteValueDictionary
                        {{"controller", "Home"}, {"action", "Error"}});

                    filterContext.HttpContext.Response.StatusCode = 401;

                    base.OnActionExecuting(filterContext);
                }
            }
        }
    }
}