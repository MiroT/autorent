﻿using AutoRent.BaseService;
using AutoRent.BaseService.Domain;
using AutoRent.Common.Interfaces;
using AutoRent.DAL.UnitOfWork;
using AutoRent.RelationalServices.Domain.CarModel;
using AutoRent.RelationalServices.Domain.PaymentModel;
using AutoRent.RelationalServices.Domain.UserModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AutoRent.CarServices
{
    public class CarRentService : BaseService<CarRent>
    {
        public CarRentService(UnitOfWork unitOfWork, IValidation modelStateWrapper)
            : base(unitOfWork, modelStateWrapper)
        {
        }

        //Method that validates some properties using the ModelStateWrapper property in the BaseService
        public override bool Validation(CarRent carRent)
        {
            if (carRent.StartDate.Date < DateTime.Now.Date)
            {
                ModelStateWrapper.Error("StartDate", "The start date has to be valid");
            }

            if (carRent.EndDate.Date < DateTime.Now.Date)
            {
                ModelStateWrapper.Error("EndDate", "The end date has to be valid");
            }

            if (carRent.StartDate.Date > carRent.EndDate.Date)
            {
                ModelStateWrapper.Error("EndDate", "The end date has to further or equal to the start date");
            }

            return ModelStateWrapper.IsValid;
        }

        //Reduces the amount of the rented car in the database
        public void ReduceTheNumberOfAvaliableCars(CarRent carRent)
        {
            Car car = UnitOfWork.GetRepo<Car>().Get((int)carRent.CarId);

            car.Amount--;

            UnitOfWork.GetRepo<Car>().Update(car);
            UnitOfWork.SaveWithoutValidation();
        }

        //Calculates the price of the rent
        public void CalculateTotalPrice(CarRent carRent)
        {
            Car car = UnitOfWork.GetRepo<Car>().Get((int)carRent.CarId);

            if (carRent.EndDate == carRent.StartDate)
            {
                carRent.TotalPrice = car.PriceOfRentPerDay;
            }
            else
            {
                carRent.TotalPrice = (car.PriceOfRentPerDay * (decimal)(carRent.EndDate - carRent.StartDate).TotalDays);
            }
        }

        //Checks if the showed CarRent is this user's
        public async Task<bool> IsUsersCarRent(CarRent carRent, BaseModel loggedUser)
        {
            CarRent currentCarRent = await GetAsync(c => c.Id == carRent.Id);

            if (currentCarRent.UserId == loggedUser.Id)
            {
                return true;
            }

            return false;
        }

        //Sets the CarRent to the original owner properties after update
        public void SetCarRentPropertiesOnUpdate(CarRent updatedCarRent, int Id)
        {
            List<CarRent> currentCarRent = GetAllNoTracking(c => c.Id == Id);

            updatedCarRent.UserId = currentCarRent[0].UserId;
            updatedCarRent.TotalPrice = currentCarRent[0].TotalPrice;
            updatedCarRent.CarId = currentCarRent[0].CarId;
        }

        //Checks the car amount on car renting
        public bool CheckIfCarAmountIsZero(int id)
        {
            Car car = UnitOfWork.GetRepo<Car>().Get(id);

            if (car.Amount == 0)
            {
                return true;
            }

            return false;
        }

        //Checks the payment method of the user on car renting
        public bool CheckIfMainPaymentIsNotSet(User user)
        {
            List<Payment> payments = UnitOfWork.GetRepo<Payment>().GetAll(p => p.UserId == user.Id);

            if (payments.Count != 0)
            {
                foreach (Payment payment in payments)
                {
                    if (payment.IsMainPaymentMethod)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
