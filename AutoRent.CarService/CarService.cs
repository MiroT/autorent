﻿using AutoRent.BaseService;
using AutoRent.Common.Interfaces;
using AutoRent.DAL.UnitOfWork;
using AutoRent.RelationalServices.Domain.CarModel;
using System.Collections.Generic;
using System.Linq;

namespace AutoRent.CarServices
{
    public class CarService : BaseService<Car>
    {
        public CarService(UnitOfWork unitOfWork, IValidation modelStateWrapper)
            : base(unitOfWork, modelStateWrapper)
        {
        }

        public override bool Validation(Car car)
        {
            return true;
        }

        //Setting all the references that the deleted car has to null in order not to delete the CarRent objects in the db
        public void SetAllReferencesToNullWhenDeleted(Car car)
        {
            List<CarRent> carRents = UnitOfWork.GetRepo<CarRent>()
                .GetAll(c => c.CarId == car.Id)
                .Select(c => 
                {
                    c.CarId = null;
                    return c;
                })
                .ToList();

            foreach (CarRent carRent in carRents)
            {
                UnitOfWork.GetRepo<CarRent>().Update(carRent);
                UnitOfWork.Save();
            }
        }
    }
}
