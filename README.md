# ProjectName: AutoRent

# Roles: User, Admin and Not logged User

# User charectaristics:
- Can create many payment methods
- Can set main payment method
- Can update/delete/see their payment methods
- Can rent as many cars as they want
- Can see all their renting documents
- Can see all the listed and avaliable cars
- Can see/update their information
- Can see About Page
- Can see Main Page
- Can see Contact Page
- Can see News Page

# Admin charectaristics:
- Can see all the users
- Can delete users
- Can see all the cars
- Can create cars
- Can update cars
- Can delete cars
- Can see all the rents
- Can delete all the rents
- Can see all the payments
- Can delete payments
- Can see About Page
- Can see Main Page
- Can see Contact Page
- Can see News Page

# Not logged user charectaristics:
- Can view Cars
- Can see Cars Details
- Can see About Page
- Can see Main Page
- Can see Contact Page
- Can see News Page
