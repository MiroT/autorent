﻿using AutoRent.BaseService.Domain;
using AutoRent.Common.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace AutoRent.UserServices.Domain
{
    public class Admin : BaseModel
    {
        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 2)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(8)]
        public string Password { get; set; }
    }
}
